# สมัครงานไม่สำเร็จ อายุอยู่ในช่วงการสมัคร ประสบการณ์น้อยกว่าที่กำหนด เพศตรง
*** Settings ***
Library    Selenium2Library

*** Variables ***


*** Test Cases ***
สมัครงานไม่สำเร็จ
    เปิดเว็บตามลิ้งค์
    พิมพ์ในช่องค้นหา "Programmer" และกดปุ่มค้นหา
    ขึ้นอาชีพมา 4 จำนวน
    เลือกตำแหน่งงานที่1
    แก้ประสบการณ์ 1 ปี
    กดสมัคร
    ระบบแสดงว่า สมัครงานไม่สำเร็จ

*** Keywords ***
เปิดเว็บตามลิ้งค์
    Open Browser          http://localhost:3000/searchJob    chrome
    Set Selenium Speed    0.5
พิมพ์ในช่องค้นหา "Programmer" และกดปุ่มค้นหา
    Input Text       id=search_text      Programmer
    Click Element    id=search_button
ขึ้นอาชีพมา 4 จำนวน
    Element Text Should Be    id=name_1    Python Programmer
เลือกตำแหน่งงานที่1
    Click Element                  id=show_detail_1
    Wait Until Element Contains    id=name             Python Programmer
แก้ประสบการณ์ 1 ปี
    Press Keys                   id=resume_experience    CTRL+a+BACKSPACE
    Input Text                   id=resume_experience    1
    Textfield Value Should Be    id=resume_experience    1
กดสมัคร
    Click Element    id=apply_job
ระบบแสดงว่า สมัครงานไม่สำเร็จ
    Element Should Contain    id=message    Not enough experience